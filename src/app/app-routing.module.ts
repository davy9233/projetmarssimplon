import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageModule } from './homepage/homepage.module';

const routes: Routes = [{
  path: '',
  loadChildren: () => import('./homepage/homepage.module').then(m => m.HomepageModule)
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

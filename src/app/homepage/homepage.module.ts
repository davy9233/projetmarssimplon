import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomepageRoutingModule } from './homepage-routing.module';
import { ModalService } from './services/modal.service';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HomepageRoutingModule
  ],
  providers:[ModalService]
})
export class HomepageModule { }

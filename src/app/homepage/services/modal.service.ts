import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  public showModal!:boolean ;

  private _showModal = new BehaviorSubject<boolean>(
    this.showModal=false
  );
  public showModal$ = this._showModal.asObservable();

  changeModal(showModal: boolean) {
    this._showModal.next(showModal);
  }
}

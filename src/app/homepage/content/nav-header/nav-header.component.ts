import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogoComponent } from 'src/app/design/logo/logo.component';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-nav-header',
  standalone: true,
  imports: [CommonModule,LogoComponent],
  templateUrl: './nav-header.component.html',
  styleUrls: ['./nav-header.component.scss']
})
export class NavHeaderComponent {

}

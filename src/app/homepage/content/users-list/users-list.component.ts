import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from 'src/app/design/user/user.component';
import { ModalComponent } from '../modal/modal.component';
import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'app-users-list',
  standalone: true,
  imports: [CommonModule,UserComponent,ModalComponent],
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent {
  public showModal!: boolean;

  constructor(private modalService:ModalService) {
    this.modalService.showModal$.subscribe(value=>this.showModal = value)
  }

}

import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhotoUserComponent } from 'src/app/design/photo-user/photo-user.component';
import { ModalService } from '../../services/modal.service';
import { ErrorClickDirective } from 'src/app/shared/directives/error-click.directive';

@Component({
  selector: 'app-modal',
  standalone: true,
  imports: [CommonModule,PhotoUserComponent,ErrorClickDirective],
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {
  constructor(private modalService:ModalService) {
  }

  public close() {
this.modalService.changeModal(false);
}
}

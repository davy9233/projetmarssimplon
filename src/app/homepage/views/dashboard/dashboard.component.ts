import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersListComponent } from '../../content/users-list/users-list.component';
import { NavHeaderComponent } from '../../content/nav-header/nav-header.component';

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [CommonModule,UsersListComponent,NavHeaderComponent],
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

}

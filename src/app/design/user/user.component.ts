import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhotoUserComponent } from '../photo-user/photo-user.component';
import { ModalService } from 'src/app/homepage/services/modal.service';

@Component({
  selector: 'app-user',
  standalone: true,
  imports: [CommonModule,PhotoUserComponent],
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent {

  constructor(private modalService:ModalService){}

  public showModal(){
  this.modalService.changeModal(true);
  }

}

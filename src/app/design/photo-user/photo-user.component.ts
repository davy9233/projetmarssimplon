import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogDirective } from 'src/app/shared/directives/dialog.directive';

@Component({
  selector: 'app-photo-user',
  standalone: true,
  imports: [CommonModule,DialogDirective],
  templateUrl: './photo-user.component.html',
  styleUrls: ['./photo-user.component.scss']
})
export class PhotoUserComponent {

}
